var SOCIView = Backbone.View.extend({
    helpers: {
        renderDate: function (date) {
            return moment(date, 'YYYY-MMDD HH:mm:ss').format('MM/DD/YYYY HH:mm');
        },
        approvalStatus: function (customerApproved, managerApproved) {
            var status = 'pending';

            if (customerApproved === '1' && managerApproved === '1') {
                status = 'approved';
            } else if (customerApproved === '-1' || managerApproved === '-1') {
                status = 'rejected';
            }

            return status;
        }
    },

    render: function () {
        if (typeof this.beforeRender === 'function') {
            this.beforeRender();
        }

        var modelData = {};
        if (this.model && this.model instanceof Backbone.Model) {
            modelData = this.model.toJSON();
        } else if (this.collection && this.collection instanceof Backbone.Collection) {
            modelData = this.collection.toJSON();
        }

        if (typeof this.template === 'function') {
            this.$el.html(this.template({
                data: modelData,
                helpers: this.helpers
            }));
        }

        if (typeof this.afterRender === 'function') {
            this.afterRender();
        }
        return this;
    }
});
