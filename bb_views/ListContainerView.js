var ListContainerView = SOCIView.extend({
    template: _.template($('#ListContainerView').text()),
    className: 'ListContainerView',

    initialize: function () {
        this.collection.applyFilter();

        this.collection.bind('remove', this.collection.applyFilter, this.collection);
        this.collection.bind('paging', this.renderList, this);
        this.collection.bind('sort', this.renderList, this);
    },

    events: {
        'click .prev-page': 'prevPage',
        'click .next-page': 'nextPage',
        'click .body .item': 'displayInfo',
        'click [sort]': 'sort',
        'click .deleteBtn': 'remove',
        'keyup input.term': 'search'
    },

    render: function () {
        var result = SOCIView.prototype.render.apply(this);

        this.renderList();

        return result;
    },
    renderList: function () {
        this.$el.find('.list').html(new ListView({
            collection: this.collection
        }).render().el);
    },

    nextPage: function () {
        this.collection.nextPage();
    },
    prevPage: function () {
        this.collection.prevPage();
    },

    sort: function (e) {
        var column = $(e.currentTarget);
        column.siblings('[sort]').removeClass('asc desc');

        var sort = {
            method: column.attr('sort'),
            asc: true
        };

        if (column.hasClass('asc')) {
            column.removeClass('asc');
            column.addClass('desc');

            sort.asc = false;
        } else {
            column.removeClass('desc');
            column.addClass('asc');
        }

        this.collection.sortState = sort;
        this.collection.applyFilter();
    },

    displayInfo: function (e) {
        e.stopImmediatePropagation();

        $('.body').append(new ItemInfoDialogView({
            model: this.collection.where({id: $(e.currentTarget).attr('itemId')})[0]
        }).render().el);
    },

    remove: function (e) {
        e.stopImmediatePropagation();

        this.collection.remove(this.collection.where({id: $(e.currentTarget).closest('.item').attr('itemId')})[0]);
    },

    search: function (e) {
        this.collection.filterTerm = e.currentTarget.value;
        this.collection.applyFilter();

        this.renderList();
    }
});