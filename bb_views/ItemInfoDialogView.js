var ItemInfoDialogView = SOCIView.extend({
    template: _.template($('#ItemInfoDialogView').text()),
    className: 'itemInfoDialog',

    events: {
        'click .overflow': 'remove',
        'click .dialog': 'onClick'
    },

    onClick: function (e) {
        e.stopImmediatePropagation();
    }
});