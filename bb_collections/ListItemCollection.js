var ListItemCollection = Backbone.Collection.extend({
    baseUrl: '/api/get_accounts',
    url: '/api/get_accounts',
    model: ListItemModel,

    total: 0,
    dateFormat: 'YYYY-MMDD HH:mm:ss',
    approval: {},
    filterTerm: null,
    originalModels: null,
    filteredModels: null,
    sortState: {
        method: null,
        asc: true
    },
    paging: {
        page: 0,
        perPage: 5,
        pagesCount: 0,
        count: 0
    },
    comparators: {
        scheduledDate: function (item1, item2) {
            return this._sortByDate(item1, item2, 'schedule');
        },
        creationDate: function (item1, item2) {
            return this._sortByDate(item1, item2, 'created_at');
        },
        author: function (item1, item2) {
            return this._sortByString(item1, item2, 'created_by_name');
        }
    },

    // Sample data for this test
    TESTDATA: {
        "status": "ok",
        "posts": [{
            "id": "1",
            "schedule": "2020-0417 17:00:00",
            "utc_offset": "420",
            "project_id": "1",
            "network": "facebook",
            "network_name": "TestFacebookPage",
            "network_thumb": "http://icons.iconarchive.com/icons/ampeross/qetto/256/facebook-icon.png",
            "message": "Test Post 1 (just text, approved)",
            "data": [],
            "customer_approved": "1",
            "manager_approved": "1",
            "rejection_message": "",
            "created_at": "2020-0413 17:41:03",
            "created_by": "admin",
            "created_by_id": "1",
            "created_by_name": "John Admin"
        }, {
            "id": "2",
            "schedule": "2020-0419 19:00:00",
            "project_id": "1",
            "network": "facebook",
            "network_name": "TestFacebookPage",
            "network_thumb": "https://scontent.xx.fbcdn.net/v/t1.0-9/17634406_1854330461448271_6787736791983791423_n.jpg?oh=e4c3a3573c0fc59359422cfd66a3865a&oe=598721E7",
            "message": "Test Post 2 (text with image, pending, rejected)  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "data": {
                "pictures": ["https://pbs.twimg.com/media/C9T6n0UUwAAOBaU.jpg"],
                "image_added": "true",
                "picture": "https://pbs.twimg.com/media/C9T6n0UUwAAOBaU.jpg",
                "type": "photo"
            },
            "customer_approved": "-1",
            "manager_approved": "1",
            "rejection_message": "",
            "rejection_message_manager": "",
            "created_at": "2020-0410 17:42:34",
            "created_by": "admin",
            "created_by_id": "1",
            "created_by_name": "Bob"
        }, {
            "id": "3",
            "schedule": "2020-0421 22:00:00",
            "network": "facebook",
            "network_name": "TestFacebookPage",
            "network_thumb": "https://scontent.xx.fbcdn.net/v/t1.0-9/17634406_1854330461448271_6787736791983791423_n.jpg?oh=e4c3a3573c0fc59359422cfd66a3865a&oe=598721E7",
            "message": "Test Post 3 (link, approved) http://www.adultswim.com/videos/rick-and-morty/",
            "data": {
                "image_added": "true",
                "pictures": ["http://i.cdn.turner.com/adultswim/big/img/2015/07/17/Rick%26MortyS02_fbsearchTn.jpg"],
                "picture": "http://i.cdn.turner.com/adultswim/big/img/2015/07/17/Rick%26MortyS02_fbsearchTn.jpg",
                "link": "http://www.adultswim.com/videos/rick-and-morty/",
                "name": "Watch Rick and Morty on Adult Swim",
                "caption": "www.adultswim.com",
                "description": "Every episode of Rick and Morty is now on AdultSwim.com for free. Rick is a mad scientist who drags his grandson, Morty, on crazy sci-fi adventures. Their escapades often have potentially harmful consequences for their family and the rest of the world. Join Rick and Morty on AdultSwim.com as they trek through alternate dimensions, explore alien planets, and terrorize Jerry, Beth, and Summer.",
                "domain": "www.adultswim.com",
                "type": "link"
            },
            "customer_approved": "1",
            "manager_approved": "1",
            "rejection_message": "",
            "created_at": "2020-0413 17:43:29",
            "created_by": "admin",
            "created_by_id": "1",
            "created_by_name": "John Admin"
        }, {
            "id": "4",
            "schedule": "2020-0424 17:00:00",
            "network": "facebook",
            "network_name": "TestFacebookPage",
            "network_thumb": "https://scontent.xx.fbcdn.net/v/t1.0-9/17634406_1854330461448271_6787736791983791423_n.jpg?oh=e4c3a3573c0fc59359422cfd66a3865a&oe=598721E7",
            "message": "Test Post 4 (text, pending)",
            "data": [],
            "customer_approved": "0",
            "manager_approved": "1",
            "rejection_message": "",
            "created_at": "2020-0413 17:43:01",
            "created_by": "admin",
            "created_by_id": "1",
            "created_by_name": "John Admin"
        }, {
            "id": "5",
            "schedule": "2020-0426 19:00:00",
            "network": "facebook",
            "network_name": "TestFacebookPage",
            "network_thumb": "https://scontent.xx.fbcdn.net/v/t1.0-9/17634406_1854330461448271_6787736791983791423_n.jpg?oh=e4c3a3573c0fc59359422cfd66a3865a&oe=598721E7",
            "message": "Test Post 5 (picture, pending)",
            "data": [],
            "customer_approved": "0",
            "manager_approved": "1",
            "rejection_message": "",
            "created_at": "2020-0418 17:44:03",
            "created_by": "admin",
            "created_by_id": "1",
            "created_by_name": "Wilson A"
        }, {
            "id": "6",
            "schedule": "2020-0428 21:00:00",
            "network": "facebook",
            "network_name": "TestFacebookPage",
            "network_thumb": "https://scontent.xx.fbcdn.net/v/t1.0-9/17634406_1854330461448271_6787736791983791423_n.jpg?oh=e4c3a3573c0fc59359422cfd66a3865a&oe=598721E7",
            "message": "Test Post 6 (link, pending) https://www.reddit.com/",
            "data": {
                "image_added": "true",
                "pictures": ["https://b.thumbs.redditmedia.com/2Hwaff37fC4f37j-3orrbjVAOVBChqbdm_dXeIhjlNw.jpg"],
                "picture": "https://b.thumbs.redditmedia.com/2Hwaff37fC4f37j-3orrbjVAOVBChqbdm_dXeIhjlNw.jpg",
                "link": "https://www.reddit.com/",
                "name": "reddit: the front page of the internet",
                "caption": "www.reddit.com",
                "description": "reddit: the front page of the internet",
                "domain": "www.reddit.com",
                "type": "link"
            },
            "customer_approved": "0",
            "manager_approved": "1",
            "rejection_message": "",
            "created_at": "2020-0413 17:41:19",
            "created_by": "admin",
            "created_by_id": "1",
            "created_by_name": "Fred Johnson"
        },
            {
                "id": "7",
                "schedule": "2020-0417 17:00:00",
                "utc_offset": "420",
                "project_id": "1",
                "network": "facebook",
                "network_name": "TestFacebookPage",
                "network_thumb": "https://scontent.xx.fbcdn.net/v/t1.0-9/17634406_1854330461448271_6787736791983791423_n.jpg?oh=e4c3a3573c0fc59359422cfd66a3865a&oe=598721E7",
                "message": "Test Post 7 (just text, approved)",
                "data": [],
                "customer_approved": "1",
                "manager_approved": "1",
                "rejection_message": "",
                "created_at": "2020-0413 17:41:03",
                "created_by": "admin",
                "created_by_id": "1",
                "created_by_name": "John Admin"
            }, {
                "id": "8",
                "schedule": "2020-0419 19:00:00",
                "project_id": "1",
                "network": "facebook",
                "network_name": "TestFacebookPage",
                "network_thumb": "https://scontent.xx.fbcdn.net/v/t1.0-9/17634406_1854330461448271_6787736791983791423_n.jpg?oh=e4c3a3573c0fc59359422cfd66a3865a&oe=598721E7",
                "message": "Test Post 8 (text with image, approved)  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                "data": {
                    "pictures": ["https://pbs.twimg.com/media/C9T6n0UUwAAOBaU.jpg"],
                    "image_added": "true",
                    "picture": "https://pbs.twimg.com/media/C9T6n0UUwAAOBaU.jpg",
                    "type": "photo"
                },
                "customer_approved": "-1",
                "manager_approved": "1",
                "rejection_message": "",
                "rejection_message_manager": "",
                "created_at": "2020-0410 17:42:34",
                "created_by": "admin",
                "created_by_id": "1",
                "created_by_name": "Bob"
            }, {
                "id": "9",
                "schedule": "2020-0421 22:00:00",
                "network": "facebook",
                "network_name": "TestFacebookPage",
                "network_thumb": "https://scontent.xx.fbcdn.net/v/t1.0-9/17634406_1854330461448271_6787736791983791423_n.jpg?oh=e4c3a3573c0fc59359422cfd66a3865a&oe=598721E7",
                "message": "Test Post 9 (link, approved) http://www.adultswim.com/videos/rick-and-morty/",
                "data": {
                    "image_added": "true",
                    "pictures": ["http://i.cdn.turner.com/adultswim/big/img/2015/07/17/Rick%26MortyS02_fbsearchTn.jpg"],
                    "picture": "http://i.cdn.turner.com/adultswim/big/img/2015/07/17/Rick%26MortyS02_fbsearchTn.jpg",
                    "link": "http://www.adultswim.com/videos/rick-and-morty/",
                    "name": "Watch Rick and Morty on Adult Swim",
                    "caption": "www.adultswim.com",
                    "description": "Every episode of Rick and Morty is now on AdultSwim.com for free. Rick is a mad scientist who drags his grandson, Morty, on crazy sci-fi adventures. Their escapades often have potentially harmful consequences for their family and the rest of the world. Join Rick and Morty on AdultSwim.com as they trek through alternate dimensions, explore alien planets, and terrorize Jerry, Beth, and Summer.",
                    "domain": "www.adultswim.com",
                    "type": "link"
                },
                "customer_approved": "1",
                "manager_approved": "1",
                "rejection_message": "",
                "created_at": "2020-0413 17:43:29",
                "created_by": "admin",
                "created_by_id": "1",
                "created_by_name": "John Admin"
            }, {
                "id": "10",
                "schedule": "2020-0424 17:00:00",
                "network": "facebook",
                "network_name": "TestFacebookPage",
                "network_thumb": "https://scontent.xx.fbcdn.net/v/t1.0-9/17634406_1854330461448271_6787736791983791423_n.jpg?oh=e4c3a3573c0fc59359422cfd66a3865a&oe=598721E7",
                "message": "Test Post 10 (text, pending)",
                "data": [],
                "customer_approved": "0",
                "manager_approved": "1",
                "rejection_message": "",
                "created_at": "2020-0413 17:43:01",
                "created_by": "admin",
                "created_by_id": "1",
                "created_by_name": "John Admin"
            }, {
                "id": "11",
                "schedule": "2020-0426 19:00:00",
                "network": "facebook",
                "network_name": "TestFacebookPage",
                "network_thumb": "https://scontent.xx.fbcdn.net/v/t1.0-9/17634406_1854330461448271_6787736791983791423_n.jpg?oh=e4c3a3573c0fc59359422cfd66a3865a&oe=598721E7",
                "message": "Test Post 11 (picture, pending)",
                "data": [],
                "customer_approved": "0",
                "manager_approved": "1",
                "rejection_message": "",
                "created_at": "2020-0418 17:44:03",
                "created_by": "admin",
                "created_by_id": "1",
                "created_by_name": "Wilson Z"
            }, {
                "id": "12",
                "schedule": "2020-0428 21:00:00",
                "network": "facebook",
                "network_name": "TestFacebookPage",
                "network_thumb": "http://www.freeiconspng.com/uploads/images-facebook-logo-png-file-page-2-19.png",
                "message": "Test Post 12 (link, rejected) https://www.reddit.com/",
                "data": {
                    "image_added": "true",
                    "pictures": ["https://b.thumbs.redditmedia.com/2Hwaff37fC4f37j-3orrbjVAOVBChqbdm_dXeIhjlNw.jpg"],
                    "picture": "https://b.thumbs.redditmedia.com/2Hwaff37fC4f37j-3orrbjVAOVBChqbdm_dXeIhjlNw.jpg",
                    "link": "https://www.reddit.com/",
                    "name": "reddit: the front page of the internet",
                    "caption": "www.reddit.com",
                    "description": "reddit: the front page of the internet",
                    "domain": "www.reddit.com",
                    "type": "link"
                },
                "customer_approved": "0",
                "manager_approved": "-1",
                "rejection_message": "",
                "created_at": "2020-0413 17:41:19",
                "created_by": "admin",
                "created_by_id": "1",
                "created_by_name": "Kate"
            }],
        "total": "12"
    },

    initialize: function (models, options) {
        this.originalModels = this.TESTDATA.posts;
        this.reset(this.TESTDATA.posts);

        this.applyFilter();
    },

    toJSON: function () {
        var result = Backbone.Collection.prototype.toJSON.apply(this);

        result.approval = this.approval;
        result.paging = this.paging;

        return result;
    },

    remove: function () {
        var item = Backbone.Collection.prototype.remove.apply(this, arguments);

        var index = _.findIndex(this.originalModels, function (model) {
            return model.id === item.id;
        });

        if (index !== -1) {
            this.originalModels.splice(index, 1);

            this.applyFilter();
        }
    },

    applyFilter: function () {
        this.filterBy(this.filterTerm);
        this.sortBy();
        this.pagination();
    },

    sortBy: function () {
        this.comparator = this.comparators[this.sortState.method];

        if (this.comparator) {
            this.sort();
        }
    },

    pagination: function () {
        var models = _.rest(this.filteredModels, this.paging.perPage * this.paging.page);
        models = _.first(models, this.paging.perPage);

        if (!models.length && this.paging.page) {
            this.paging.page = 0;

            this.pagination();
        } else {
            this.reset(models);
            this.trigger('paging');
        }
    },
    nextPage: function () {
        if (this.paging.page < this.paging.pagesCount) {
            this.paging.page++;

            this.pagination();
        }
    },
    prevPage: function () {
        if (this.paging.page > 0) {
            this.paging.page--;

            this.pagination();
        }
    },

    filterBy: function (term) {
        var models = this.originalModels;

        if (term && term.length >= 3) {
            term = term.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');

            models = _.filter(this.originalModels, function (item) {
                return !!_.find(['message', 'created_by_name'], function (key) {
                    return new RegExp(term, 'gi').test(item[key]);
                });
            });
        }

        this.reset(models);

        this.filteredModels = this.models;

        this._calculateData();
    },

    _calculateData: function () {
        this.paging.count = this.length;
        this.paging.pagesCount = Math.ceil(this.length / this.paging.perPage);

        this.approval = {approved: 0, pending: 0, rejected: 0};

        var self = this;
        _.each(this.models, function (model) {
            if (model.get('customer_approved') === '1' && model.get('manager_approved') === '1') {
                self.approval.approved++;
            } else if (model.get('customer_approved') === '-1' || model.get('manager_approved') === '-1') {
                self.approval.rejected++;
            } else {
                self.approval.pending++;
            }
        });
    },

    _sortByDate: function (item1, item2, property) {
        return this._sort(moment(item1.get(property), this.dateFormat) - moment(item2.get(property), this.dateFormat));
    },

    _sortByString: function (item1, item2, property) {
        return this._sort(item1.get(property).localeCompare(item2.get(property)));
    },

    _sort: function (value) {
        return (this.sortState.asc ? 1 : -1) * value;
    }
});